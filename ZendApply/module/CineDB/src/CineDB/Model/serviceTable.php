<?php

namespace CineDB\Model;

use Zend\Db\TableGateway\TableGateway;

class serviceTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function saveservice(service $service)
	{
		$data = array(
			'type' => $service->type,
			'start' => $service->start,
			'expire' => $service->expire,
			'usernameUser' => $service->usernameUser,
			'actualconsumption' => $service->actualconsumption,
			'maxconsumption' => $service->maxconsumption,
			);

		$id = (int)$service->id;

		if ($id == 0) {
			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {
			if ($this->getservice($id)) {
			$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function getservice($id)
	{
		$rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return False;
        }
        return $row;

	}

	public function getservicesofuser($usernameUser)
	{
		$rowset = $this->tableGateway->select(array('usernameUser' => $usernameUser));
        $row = $rowset->current();
        if (!$row) {
            return False;
        }
        return $row;
	}

	public function getservicewithIdUsername($id, $usernameUser)
	{
		$rowset = $this->tableGateway->select(array('id' => $id, 'usernameUser' => $usernameUser));
        $row = $rowset->current();
        if (!$row) {
            return False;
        }
        return $row;
	}
}