<?php

namespace CineDB\Model;

use Zend\Db\TableGateway\TableGateway;

class moviehistoryTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function savemovieHistory(moviehistory $moviehistory)
	{
		$data = array(
			'title' => $moviehistory->title,
			'year' => $moviehistory->year,
			'runtime' => $moviehistory->runtime,
			'genre' => $moviehistory->genre,
			'director' => $moviehistory->director,
			'actors' => $moviehistory->actors,
			'plot' => $moviehistory->plot,
			'poster' => $moviehistory->poster,
			'score' => $moviehistory->score,
			'idService' => $moviehistory->idService,
			);

		$id = (int)$moviehistory->id;

		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getmovieHistory($id)) {
			$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function gethistoryofservice($idService)
	{
		$rowset = $this->tableGateway->select(array('idService' => $idService));

		$rows = array();
		foreach ($rowset as $row){
            $rows[] = $row;
        }
        //$row = $rowset->current();
        if (!$rows) {
           	return false;
        }
        return $rows;
	}

	public function ismovieExistOnService($title, $idService)
	{
		$rowset = $this->tableGateway->select(array('title' => $title, 'idService' => $idService));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
	}

	public function fillmovieHistory($movie, $idService)
	{
		$movie['idService'] = $idService;

		return $movie;
	}
}