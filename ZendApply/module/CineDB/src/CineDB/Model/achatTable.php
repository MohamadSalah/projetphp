<?php

namespace CineDB\Model;

use Zend\Db\TableGateway\TableGateway;

class achatTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function saveachat(achat $achat)
	{
		$data = array(
			'type' => $achat->type,
			'usernameUser' => $achat->usernameUser,
			);

		$id = (int)$achat->id;

		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getachat($id)) {
			$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function getachatofuser($usernameUser)
	{
		$rowset = $this->tableGateway->select(array('usernameUser' => $usernameUser));

		$rows = array();
		foreach ($rowset as $row){
            $rows[] = $row;
        }
        //$row = $rowset->current();
        if (!$rows) {
           	return false;
        }
        return $rows;
	}

	public function fillachat($type, $usernameUser)
	{
		$hash = array();
		$hash['type'] = $type;
		$hash['usernameUser'] = $usernameUser;

		return $hash;
	}
}