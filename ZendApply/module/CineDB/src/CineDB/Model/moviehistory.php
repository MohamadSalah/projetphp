<?php

namespace CineDB\Model;

class moviehistory
{
	public $id;
	public $title;
	public $year;
	public $runtime;
	public $genre;
	public $director;
	public $actors;
	public $plot;
	public $poster;
	public $score;
	public $idService;
	
	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id'] : null;
		$this->title = (isset($data['title'])) ? $data['title'] : null;
		$this->year = (isset($data['year'])) ? $data['year'] : null;
		$this->runtime = (isset($data['runtime'])) ? $data['runtime'] : null;
		$this->genre = (isset($data['genre'])) ? $data['genre'] : null;
		$this->director = (isset($data['director'])) ? $data['director'] : null;
		$this->actors = (isset($data['actors'])) ? $data['actors'] : null;
		$this->plot = (isset($data['plot'])) ? $data['plot'] : null;
		$this->poster = (isset($data['poster'])) ? $data['poster'] : null;
		$this->score = (isset($data['score'])) ? $data['score'] : null;
		$this->idService = (isset($data['idService'])) ? $data['idService'] : null;
	}
}

?>