<?php

namespace CineDB\Model;

use Zend\Db\TableGateway\TableGateway;

class userTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function saveuser(user $user)
	{
		$data = array(
			'username' => $user->username,
			'password' => $user->password,
			);

		$id = (int)$user->id;

		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getuser($id)) {
			$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function getuserbyusername($username)
	{
        $rowset = $this->tableGateway->select(array('username' => $username));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $username");
        }
        return $row;
	}
}