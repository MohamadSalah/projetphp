<?php

namespace CineDB\Model;

class consumption
{
	public $id;
	public $idService;
	public $actual;
	public $max;
	
	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id'] : null;
		$this->idService = (isset($data['idService'])) ? $data['idService'] : null;
		$this->actual = (isset($data['actual'])) ? $data['actual'] : null;
		$this->max = (isset($data['max'])) ? $data['max'] : null;
	}
}

?>