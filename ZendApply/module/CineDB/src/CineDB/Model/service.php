<?php

namespace CineDB\Model;

class service
{
	public $id;
	public $type;
	public $start;
	public $expire;
	public $usernameUser;
	public $actualconsumption;
	public $maxconsumption;
	
	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id'] : null;
		$this->type = (isset($data['type'])) ? $data['type'] : null;
		$this->start = (isset($data['start'])) ? $data['start'] : null;
		$this->expire = (isset($data['expire'])) ? $data['expire'] : null;
		$this->usernameUser = (isset($data['usernameUser'])) ? $data['usernameUser'] : null;
		$this->actualconsumption = (isset($data['actualconsumption'])) ? $data['actualconsumption'] : null;
		$this->maxconsumption = (isset($data['maxconsumption'])) ? $data['maxconsumption'] : null;
	}
}

?>