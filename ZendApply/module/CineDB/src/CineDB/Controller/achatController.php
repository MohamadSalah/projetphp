<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class achatController extends AbstractActionController
{
	protected $achatTable;

	public function indexAction()
	{
		if ($user = $this->identity()) {
			$achats = $this->getachatTable()->getachatofuser($user->username);
			if ($achats)
			{
				return new ViewModel(array(
					'achats' => $achats,
				));
			}
			else
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
		}
		else
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
	}

	public function getachatTable()
	{
		if (!$this->achatTable) {
			$sm = $this->getServiceLocator();
			$this->achatTable = $sm->get('CineDB\Model\achatTable');
		}
		return $this->achatTable;
	}
}