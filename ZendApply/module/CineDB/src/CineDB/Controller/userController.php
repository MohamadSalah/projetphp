<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class userController extends AbstractActionController
{
	protected $userTable;

	public function indexAction()
	{
		return new ViewModel(array(
			'users' => $this->getuserTable()->fetchAll(),
		));
	}

	public function getuserTable()
	{
		if (!$this->userTable) {
			$sm = $this->getServiceLocator();
			$this->userTable = $sm->get('CineDB\Model\userTable');
		}
		return $this->userTable;
	}

	public function getuser($username)
	{
		$user = $this->userTable->getuserbyusername($username);

		return $user;
	}

	public function saveuser(user $user)
	{
		$this->userTable->saveuser($user);
	}

	public function addAction()
	{
	}

	public function editAction()
	{
	}
	
	public function deleteAction()
	{
	}
}