<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use CineDB\Model\moviehistory;

class moviehistoryController extends AbstractActionController
{
	protected $movieHistoryTable;

	/*public function indexAction()
	{
		return new ViewModel(array(
			'movieHistorys' => $this->getmovieHistoryTable()->fetchAll(),
		));
	}*/

	public function getmovieHistoryTable()
	{
		if (!$this->movieHistoryTable) {
			$sm = $this->getServiceLocator();
			$this->movieHistoryTable = $sm->get('CineDB\Model\moviehistoryTable');
		}
		return $this->movieHistoryTable;
	}

	public function savemovieHistory(movieHistory $movieHistory)
	{
		$this->getmovieHistoryTable()->savemovieHistory($movieHistory);
	}

	public function fillmovieHistory($title, $idService)
	{
		$hash = array();
		$hash['title'] = $title;
		$hash['idService'] = $idService;

		return $hash;
	}

	public function ismovieExistOnService($title, $idService){
		return $this->getmovieHistoryTable()->ismovieExistOnService($title, $idService);
	}
}