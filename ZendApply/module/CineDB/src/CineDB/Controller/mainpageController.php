<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use CineDB\Form\ServiceForm;
use CineDB\Form\ServiceFilter;

use CineDB\Model\service;
use CineDB\Model\consumption;

class mainpageController extends AbstractActionController
{
	protected $serviceTable;

	public function indexAction()
	{
		if ($user = $this->identity()) {
			$this->getserviceTable();
			$services = $this->serviceTable->getservicesofuser($user->username);
			if ($services)
			{
				return new ViewModel(array(
					'services' => $services,
				));
			}
			else
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'chooseservice'));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}

	public function getserviceTable()
	{
		if (!$this->serviceTable) {
			$sm = $this->getServiceLocator();
			$this->serviceTable = $sm->get('CineDB\Model\serviceTable');
		}
		return $this->serviceTable;
	}

	public function getservicesofuser($idUser)
	{
		$this->serviceTable->getservicesofuser($idUser);
	}

	public function saveservice(service $service)
	{
		$this->serviceTable->saveservice($user);
	}

	public function chooseserviceAction()
	{
		if ($user = $this->identity()) {
			$this->getserviceTable();
			$services = $this->serviceTable->getservicesofuser($user->username);
			if ($services)
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
			}
			else
			{
				$form = new ServiceForm();
				$form->get('submitFree')->setValue('TakeFreeService');
				$form->get('submitSilver')->setValue('TakeSilverService');
				$form->get('submitGold')->setValue('TakeGoldService');

				$request = $this->getRequest();
        		if ($request->isPost()) {
        			$authFormFilters = new ServiceFilter();
					$form->setInputFilter($authFormFilters->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
	        			$data = $form->getData();
	        			if ($data['submitFree'])
	        			{
	        				$service = new service();
	        				$hashservice = $this->fillDataArray(0, date("Y-m-d H:i:s"), 1, $user->username, 0, 10);
							$service->exchangeArray($hashservice);
							$idService = $this->getserviceTable()->saveservice($service);
	        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        			}
	        			if ($data['submitSilver'])
	        			{
	        				$service = new service();
	        				$hashservice = $this->fillDataArray(1, date("Y-m-d H:i:s"), 1, $user->username, 0, 100);
							$service->exchangeArray($hashservice);
							$idService = $this->getserviceTable()->saveservice($service);
	        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        			}
	        			if ($data['submitGold'])
	        			{
	        				$service = new service();
	        				$hashservice = $this->fillDataArray(2, date("Y-m-d H:i:s"), 1, $user->username, 0, 1000);
							$service->exchangeArray($hashservice);
							$idService = $this->getserviceTable()->saveservice($service);
	        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        			}
        			}
        		}

        		return new ViewModel(array('form' => $form));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}


	public function fillDataArray($type, $start, $expire, $usernameUser, $actualconsumption, $maxconsumption)
	{
		$today = $start;
		$expire = date('Y-m-d',strtotime('+'.$expire.' month',strtotime($today)));
		$hash = array();
		$hash['type'] = $type;
		$hash['start'] = $start;
		$hash['expire'] = $expire;
		$hash['usernameUser'] = $usernameUser;
		$hash['actualconsumption'] = $actualconsumption;
		$hash['maxconsumption'] = $maxconsumption;

		return $hash;
	}

	public function addAction()
	{
	}

	public function editAction()
	{
	}
	
	public function deleteAction()
	{
	}
}