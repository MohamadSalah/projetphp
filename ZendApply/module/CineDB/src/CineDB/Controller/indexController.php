<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

use CineDB\Form\LoginForm;

class IndexController extends AbstractActionController
{
	function init()
	{
		$this->initView();
		Zend_Loader::loadClass('Album');
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->user = Zend_Auth::getInstance()->getIdentity();
	}

	function preDispatch()
	{
		$auth = Zend_Auth::getInstance();
	 
		if (!$auth->hasIdentity()) {
			$this->_redirect('auth/login');
		}
	}
}