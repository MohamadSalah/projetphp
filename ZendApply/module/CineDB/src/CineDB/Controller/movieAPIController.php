<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class movieAPIController extends AbstractActionController
{
	public function indexAction()
	{
		//return new ViewModel(array(
		//	'consumptions' => $this->getconsumptionTable()->fetchAll(),
		//));
	}

	public function getsearchmovie($query)
	{
		/*if (!$this->consumptionTable) {
			$sm = $this->getServiceLocator();
			$this->consumptionTable = $sm->get('CineDB\Model\consumptionTable');
		}
		return $this->consumptionTable;*/
		$searchurl = 'http://www.omdbapi.com/?s='.$query.'&type=movie';
		$json = file_get_contents($searchurl);

		$jsonhashmap = json_decode($json);

		$listmovies = array();

		foreach ($jsonhashmap as $movie)
		{
			array_push($listmovies, $movie['Title']);
		}

		return $listmovies;
	}

	public function getmovie($moviename)
	{
		$movieurl = 'http://www.omdbapi.com/?t='.$moviename.'&type=movie';
		$json = file_get_contents($movieurl);
		$jsonhashmapmovie = json_decode($json);

		//check comsuption actutel, si ok augmenter consuption, mettre dans historique puis return, sinon non.

		return $jsonhashmapmovie;
	}

	public function addAction()
	{
	}

	public function editAction()
	{
	}
	
	public function deleteAction()
	{
	}
}