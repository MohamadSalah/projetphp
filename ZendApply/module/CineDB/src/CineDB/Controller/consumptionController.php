<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class consumptionController extends AbstractActionController
{
	protected $consumptionTable;

	public function indexAction()
	{
		return new ViewModel(array(
			'consumptions' => $this->getconsumptionTable()->fetchAll(),
		));
	}

	public function getconsumptionTable()
	{
		if (!$this->consumptionTable) {
			$sm = $this->getServiceLocator();
			$this->consumptionTable = $sm->get('CineDB\Model\consumptionTable');
		}
		return $this->consumptionTable;
	}

	public function getconsumptionofservice($idService)
	{
		$this->consumptionTable->getconsumptionofservice($idService);
	}

	public function saveconsumption(consumption $consumption)
	{
		$this->consumptionTable->saveconsumption($consumption);
	}

	public function addAction()
	{
	}

	public function editAction()
	{
	}
	
	public function deleteAction()
	{
	}
}