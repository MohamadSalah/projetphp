<?php

namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use CineDB\Form\ServiceForm;
use CineDB\Form\ServiceFilter;

use CineDB\Form\SearchMovieForm;
use CineDB\Form\SearchMovieFilter;

use CineDB\Form\OptionForm;
use CineDB\Form\OptionFilter;

use CineDB\Model\service;
use CineDB\Model\consumption;
use CineDB\Model\moviehistory;
use CineDB\Model\achat;


use CineDB\Model\moviehistoryTable;

class serviceController extends AbstractActionController
{
	protected $serviceTable;

	public function indexAction()
	{
		if ($user = $this->identity()) {
			$this->getserviceTable();
			$services = $this->serviceTable->getservicesofuser($user->username);
			if ($services)
			{
				return new ViewModel(array(
					'services' => $services,
				));
			}
			else
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'chooseservice'));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}

		/*return new ViewModel(array(
			'services' => $this->getserviceTable()->fetchAll(),
		));*/
	}

	public function getserviceTable()
	{
		if (!$this->serviceTable) {
			$sm = $this->getServiceLocator();
			$this->serviceTable = $sm->get('CineDB\Model\serviceTable');
		}
		return $this->serviceTable;
	}

	public function getservicesofuser($idUser)
	{
		$this->serviceTable->getservicesofuser($idUser);
	}

	public function saveservice(service $service)
	{
		$this->serviceTable->saveservice($user);
	}

	public function chooseserviceAction()
	{
		if ($user = $this->identity()) {
			$this->getserviceTable();
			$services = $this->serviceTable->getservicesofuser($user->username);
			if ($services)
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
			}
			else
			{
				$form = new ServiceForm();
				$form->get('submitFree')->setValue('Take Free Service : 10 sheet for 1 month : free');
				$form->get('submitSilver')->setValue('Take Silver Service: 100 sheet for 1 month : 3€');
				$form->get('submitGold')->setValue('Take Gold Service : 1000 sheet for 1 month : 5€');

				$request = $this->getRequest();
        		if ($request->isPost()) {
        			$authFormFilters = new ServiceFilter();
					$form->setInputFilter($authFormFilters->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
	        			$data = $form->getData();
	        			if ($data['submitFree'])
	        			{
	        				$service = new service();
	        				$hashservice = $this->fillDataArray(0, date("Y-m-d H:i:s"), 1, $user->username, 0, 10);
							$service->exchangeArray($hashservice);
							$idService = $this->getserviceTable()->saveservice($service);
							$this->saveachat('Free Service', $user->username);

	        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        			}
	        			if ($data['submitSilver'])
	        			{
	        				$service = new service();
	        				$hashservice = $this->fillDataArray(1, date("Y-m-d H:i:s"), 1, $user->username, 0, 100);
							$service->exchangeArray($hashservice);
							$idService = $this->getserviceTable()->saveservice($service);
							$this->saveachat('Silver Service', $user->username);
	        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        			}
	        			if ($data['submitGold'])
	        			{
	        				$service = new service();
	        				$hashservice = $this->fillDataArray(2, date("Y-m-d H:i:s"), 1, $user->username, 0, 1000);
							$service->exchangeArray($hashservice);
							$idService = $this->getserviceTable()->saveservice($service);
							$this->saveachat('Gold Service', $user->username);
	        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        			}
        			}
        		}

        		return new ViewModel(array('form' => $form));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}

	public function searchmovieAction()
	{
		if ($user = $this->identity()) {
			$this->getserviceTable();
			$services = $this->serviceTable->getservicesofuser($user->username);
			if ($services && $service = $this->checkCurrentService($services))
			{
				$form = new SearchMovieForm();
				$form->get('submit')->setValue('Search movies');
				$consumption = $service->maxconsumption - $service->actualconsumption;

				$request = $this->getRequest();
        		if ($request->isPost()) {
        			$authFormFilters = new SearchMovieFilter();
					$form->setInputFilter($authFormFilters->getInputFilter());
					$form->setData($request->getPost());

					if ($form->isValid()) {
	        			$data = $form->getData();
	        			if ($listmovies = $this->getsearchmovie($data['search']))
		        			return new ViewModel(array('form' => $form, 'listmovies' => $listmovies, 'consumption' => $consumption));
		        		else
		        			return new ViewModel(array('form' => $form, 'listmovies' => null, 'consumption' => $consumption));
        			}
        		}
        		return new ViewModel(array('form' => $form, 'listmovies' => null, 'consumption' => $consumption));
        	}
			else
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'chooseservice'));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}

	public function getmovieAction()
	{
		if ($user = $this->identity()) 
		{
			
			$services = $this->getserviceTable()->getservicesofuser($user->username);
			if ($services && $service = $this->checkCurrentService($services))
			{
				$moviename = $this->params()->fromRoute('id');
				if ($moviename)
				{
					$sm = $this->getServiceLocator();
					$moviehistoryControl = $sm->get('CineDB\Model\moviehistoryTable');
					$movieExistOnService = $moviehistoryControl->ismovieExistOnService($moviename, $service->id);
					if ($service->actualconsumption < $service->maxconsumption || $movieExistOnService)
					{
			        	if ($movieExistOnService == false)
			        	{
			        		$movie = $this->getmovie(urlencode($moviename));
			        		if ($movie)
			        		{
					        	$service->actualconsumption = $service->actualconsumption + 1;
					        	$this->getserviceTable()->saveservice($service);

					        	$movieHistory = new moviehistory();
					        	$hashhistory = $moviehistoryControl->fillmovieHistory($movie, $service->id);
								$movieHistory->exchangeArray($hashhistory);

					        	$moviehistoryControl->savemovieHistory($movieHistory);
					        	$consumption = $service->maxconsumption - $service->actualconsumption;
		        				return new ViewModel(array('movie' => $movie, 'consumption' => $consumption));

				        	}
				        	else
				        		return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
			        	}

			        	$movie = $this->convertmovieHistoryArrayToArray($movieExistOnService);
			        	$consumption = $service->maxconsumption - $service->actualconsumption;
		        		return new ViewModel(array('movie' => $movie, 'consumption' => $consumption));
	        		}
	        		else
	        			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        	}
	        	else
	        	{
        			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
        		}
        	}
			else
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'chooseservice'));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}

	public function historyAction()
	{
		if ($user = $this->identity()) {
			$services = $this->getserviceTable()->getservicesofuser($user->username);
			if ($services)
			{
				$idService = $this->params()->fromRoute('id');
				if ($idService && $service = $this->getserviceTable()->getservicewithIdUsername($idService, $user->username))
				{
		        	$sm = $this->getServiceLocator();
					$moviehistoryTable= $sm->get('CineDB\Model\moviehistoryTable');

					$listmovies = $moviehistoryTable->gethistoryofservice($idService);

					if ($listmovies)
						return new ViewModel(array('listmovies' => $listmovies));
					return new ViewModel(array('listmovies' => null));
	        	}
	        	else
	        		return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        }
			else
			{
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'chooseservice'));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}

	public function optionAction()
	{
		if ($user = $this->identity()) 
		{
			$this->getserviceTable();
			$services = $this->serviceTable->getservicesofuser($user->username);
			if ($services)
			{
				$idService = $this->params()->fromRoute('id');
				if ($idService && $service = $this->getserviceTable()->getservicewithIdUsername($idService, $user->username))
				{
					$form = new OptionForm();
					$form->get('submitMoreMovie')->setValue('+ 10 movies sheet : 1€');
					$form->get('submitSilver')->setValue('Upgrade To Silver Service : 3€ and 1 month extend');
					$form->get('submitGold')->setValue('Upgrade To Gold Service : 5€ and 1 month extend ');

					$request = $this->getRequest();
	        		if ($request->isPost()) 
	        		{
	        			$authFormFilters = new OptionFilter();
						$form->setInputFilter($authFormFilters->getInputFilter());
						$form->setData($request->getPost());

						if ($form->isValid()) {
		        			$data = $form->getData();
		        			if ($data['submitMoreMovie'])
		        			{
		        				$service->maxconsumption = $service->maxconsumption + 10;
			        			$this->getserviceTable()->saveservice($service);
			        			$this->saveachat('More movie option', $user->username);
		        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
		        			}
		        			if ($data['submitSilver'])
		        			{
		        				$service->type = 1;
		        				$service->maxconsumption = $service->maxconsumption + 100;
		        				$service->expire = date('Y-m-d',strtotime('+1 month',strtotime($service->expire)));
			        			$this->getserviceTable()->saveservice($service);
			        			$this->saveachat('Upgrade to Silver service', $user->username);
		        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
		        			}
		        			if ($data['submitGold'])
		        			{
		        				$service->type = 2;
		        				$service->maxconsumption = $service->maxconsumption + 1000;
		        				$service->expire = date('Y-m-d',strtotime('+1 month',strtotime($service->expire)));
			        			$this->getserviceTable()->saveservice($service);
			        			$this->saveachat('Upgrade to Gold service', $user->username);
		        				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
		        			}
	        			}
	        		}
	        		return new ViewModel(array('form' => $form, 'idService' => $idService));
	        	}
	        	else
	        	{
	        		return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
	        	}
			}
			else
			{
        		return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'chooseservice'));
			}
		}
		else
		{
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));
		}
	}

	public function fillDataArray($type, $start, $expire, $usernameUser, $actualconsumption, $maxconsumption)
	{
		$today = $start;
		$expire = date('Y-m-d',strtotime('+'.$expire.' month',strtotime($today)));
		$hash = array();
		$hash['type'] = $type;
		$hash['start'] = $start;
		$hash['expire'] = $expire;
		$hash['usernameUser'] = $usernameUser;
		$hash['actualconsumption'] = $actualconsumption;
		$hash['maxconsumption'] = $maxconsumption;

		return $hash;
	}

	public function getsearchmovie($query)
	{
		$searchurl = 'http://www.omdbapi.com/?s='.$query.'&type=movie';
		$json = file_get_contents($searchurl);
		$jsonhashmap = json_decode($json, true);

		if (isset($jsonhashmap['Search']))
		{
			$jsonarraymovies = $jsonhashmap['Search'];
			$listmovies = array();
			foreach ($jsonarraymovies as $movie)
			{
				array_push($listmovies, $movie['Title']);
			}
			return $listmovies;
		}
		else
			return false;
	}

	public function getmovie($moviename)
	{
		$movieurl = 'http://www.omdbapi.com/?t='.$moviename.'&type=movie';
		$json = file_get_contents($movieurl);
		if ($json){
			$jsonhashmapmovie = json_decode($json);
			if ($jsonhashmapmovie)
			{
				$movie = array();

				$movie['title'] = $jsonhashmapmovie->{'Title'};
				$movie['year'] = $jsonhashmapmovie->{'Year'};
				$movie['runtime'] = $jsonhashmapmovie->{'Runtime'};
				$movie['genre'] = $jsonhashmapmovie->{'Genre'};
				$movie['director'] = $jsonhashmapmovie->{'Director'};
				$movie['actors'] = $jsonhashmapmovie->{'Actors'};
				$movie['plot'] = $jsonhashmapmovie->{'Plot'};
				$movie['poster'] = $jsonhashmapmovie->{'Poster'};
				$movie['score'] = $jsonhashmapmovie->{'imdbRating'};

				return $movie;
			}
		}
		return false;
	}

	public function checkCurrentService($services)
	{
		if (is_array($services))
		{
			foreach ($services as $service) {
				if (strtotime($services->expire) >= strtotime(date("Y-m-d H:i:s")))
					return $services;
			}
			return false;
		}
		else
		{
			if (strtotime($services->expire) >= strtotime(date("Y-m-d H:i:s")))
				return $services;
			else
				return false;
		}
	}

	public function saveachat($type, $usernameUser)
	{
		$sm = $this->getServiceLocator();
		$achatTable= $sm->get('CineDB\Model\achatTable');
		$achat = new achat();

		$hashachat = $achatTable->fillachat($type, $usernameUser);
		$achat->exchangeArray($hashachat);
		$achatTable->saveachat($achat);
	}

	public function convertmovieHistoryArrayToArray($movieh)
	{
		$movie = array();

		$movie['title'] = $movieh->title;
		$movie['year'] = $movieh->year;
		$movie['runtime'] = $movieh->runtime;
		$movie['genre'] = $movieh->genre;
		$movie['director'] = $movieh->director;
		$movie['actors'] = $movieh->actors;
		$movie['plot'] = $movieh->plot;
		$movie['poster'] = $movieh->poster;
		$movie['score'] =$movieh->score;

		return $movie;
	}
}