<?php
namespace CineDB\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use CineDB\Model\user;
use CineDB\Form\RegistrationForm;
use CineDB\Form\Registration;

use CsnBase\Zend\Validator\ConfirmPassword;

class RegistrationController extends AbstractActionController
{
	protected $userTable;
	
	public function indexAction()
	{
		// A test instantiation to make sure it works. Not used in the application. You can remove the next line
		// $myValidator = new ConfirmPassword();
		$form = new RegistrationForm();
		$form->get('submit')->setValue('Register');
		
		$request = $this->getRequest();
        if ($request->isPost()) {
			$form->setInputFilter(new Registration($this->getServiceLocator()));
			$form->setData($request->getPost());
			 if ($form->isValid()) {
				$data = $form->getData();
				//$data = $this->prepareData($data);
				$auth = new User();
				$auth->exchangeArray($data);

				$this->getUserTable()->saveuser($auth);
				
				return $this->redirect()->toRoute('cinedb/default', array('controller'=>'registration', 'action'=>'success'));					
			}			 
		}
		return new ViewModel(array('form' => $form));
	}

	public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('CineDB\Model\userTable');
        }
        return $this->userTable;
    }

    public function successAction()
    {
		return new ViewModel();
	}
}