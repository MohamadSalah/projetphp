<?php
namespace CineDB\Controller;

use CineDB\Model\User; // Ajouter ces uses
use CineDB\Form\Login;
use CineDB\Form\LoginForm;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

class LoginController extends AbstractActionController
{

	public function indexAction()
    {
		
		return $this->redirect()->toRoute('cinedb/default', array('controller'=>'login', 'action'=>'login'));	
	}

	public function loginAction()
	{
		$user = $this->identity();
		if ($user)
			return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
		$form = new LoginForm(); 
		$form->get('submit')->setValue('Login');
		$messages = null;

		$request = $this->getRequest();
		if ($request->isPost())
		{
			$authFormFilters = new Login();
			$form->setInputFilter($authFormFilters->getInputFilter());	
			$form->setData($request->getPost());

			if ($form->isValid()) {
				$data = $form->getData();
				$sm = $this->getServiceLocator();
				$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
				
				$config = $this->getServiceLocator()->get('Config');

				$authAdapter = new AuthAdapter($dbAdapter,
										   'user',
										   'username',
										   'password'
										  );
				$authAdapter->setIdentity($data['username'])->setCredential($data['password']);
				$auth = new AuthenticationService();
				$result = $auth->authenticate($authAdapter);

				switch ($result->getCode()) {
					case Result::FAILURE_IDENTITY_NOT_FOUND:
						// do stuff for nonexistent identity
						break;
					case Result::FAILURE_CREDENTIAL_INVALID:
						// do stuff for invalid credential
						break;
					case Result::SUCCESS:
						$storage = $auth->getStorage();
						$storage->write($authAdapter->getResultRowObject(
							null,
							'password'
						));
						$time = 1209600; // 14 days 1209600/3600 = 336 hours => 336/24 = 14 days
						return $this->redirect()->toRoute('cinedb/default', array('controller'=>'service', 'action'=>'index'));
						/*if ($data['rememberme']) {
							$sessionManager = new \Zend\Session\SessionManager();
							$sessionManager->rememberMe($time);
						}*/
						break;
					default:
						// do stuff for other failure
						break;
				}
				foreach ($result->getMessages() as $message) {
					$messages .= "$message\n"; 
				}
			}
		}

		return new ViewModel(array('form' => $form, 'messages' => $messages));
	}

	public function logoutAction()
	{
		$auth = new AuthenticationService();
		
		if ($auth->hasIdentity()) {
			$identity = $auth->getIdentity();
		}			
		
		$auth->clearIdentity();
		
		return $this->redirect()->toRoute('cinedb/default', array('controller' => 'login', 'action' => 'login'));		
	}	
}