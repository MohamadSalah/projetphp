<?php

namespace CineDB\Form;
use Zend\Form\Form;

class OptionForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('service');
		$this->setAttribute('method', 'post');

		$this->add(array(
				'name' => 'submitMoreMovie',
				'type' => 'Submit',
					'attributes' => array(
					'value' => 'MoreMovie',
					'id' => 'submitbutton',
				),
		));

		$this->add(array(
				'name' => 'submitSilver',
				'type' => 'Submit',
					'attributes' => array(
					'value' => 'Silver',
					'id' => 'submitbutton',
				),
		));

		$this->add(array(
				'name' => 'submitGold',
				'type' => 'Submit',
					'attributes' => array(
					'value' => 'Gold',
					'id' => 'submitbutton',
				),
		));
	}
}