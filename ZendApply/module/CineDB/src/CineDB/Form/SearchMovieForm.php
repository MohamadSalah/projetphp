<?php

namespace CineDB\Form;
use Zend\Form\Form;

class SearchMovieForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('service');
		$this->setAttribute('method', 'post');
 
        $this->add(
             array(
             	'name' => 'search',
             	'attributes' => array(
	                'type' => 'Text',
	                'label' => 'Search'
	            ),
        ));
 
        $this->add(array(
				'name' => 'submit',
				'type' => 'Submit',
					'attributes' => array(
					'value' => 'Go',
					'id' => 'submitbutton',
				),
		));

	}
}