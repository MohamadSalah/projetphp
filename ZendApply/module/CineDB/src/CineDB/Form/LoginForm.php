<?php

namespace CineDB\Form;
use Zend\Form\Form;

class LoginForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('user');
		$this->setAttribute('method', 'post');
 
        $this->add(
             array(
             	'name' => 'username',
             	'attributes' => array(
	                'type' => 'Text',
	                'label' => 'Username'
	            ),
        ));
 
        $this->add(array(
	        	'name' => 'password',
	        	'attributes' => array(
	                'type' => 'Password',
	                'label' => 'Password',
	            ),
        ));
 
        $this->add(array(
				'name' => 'submit',
				'type' => 'Submit',
					'attributes' => array(
					'value' => 'Go',
					'id' => 'submitbutton',
				),
		));

	}
}