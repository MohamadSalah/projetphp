<?php

namespace CineDB;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use CineDB\Model\user;
use CineDB\Model\userTable;
use CineDB\Model\service;
use CineDB\Model\serviceTable;
use CineDB\Model\moviehistory;
use CineDB\Model\moviehistoryTable;
use CineDB\Model\achat;
use CineDB\Model\achatTable;


class Module
{
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php',
			),
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'CineDB\Model\userTable' => function($sm) {
					$tableGateway = $sm->get('userTableGateway');
					$table = new userTable($tableGateway);
					return $table;
				},
				'userTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new user());
					return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
				},
				'CineDB\Model\serviceTable' => function($sm) {
					$tableGateway = $sm->get('serviceTableGateway');
					$table = new serviceTable($tableGateway);
					return $table;
				},
				'serviceTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new service());
					return new TableGateway('service', $dbAdapter, null, $resultSetPrototype);
				},
				'CineDB\Model\moviehistoryTable' => function($sm) {
					$tableGateway = $sm->get('moviehistoryTableGateway');
					$table = new moviehistoryTable($tableGateway);
					return $table;
				},
				'moviehistoryTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new moviehistory());
					return new TableGateway('moviehistory', $dbAdapter, null, $resultSetPrototype);
				},
				'CineDB\Model\achatTable' => function($sm) {
					$tableGateway = $sm->get('achatTableGateway');
					$table = new achatTable($tableGateway);
					return $table;
				},
				'achatTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new achat());
					return new TableGateway('achat', $dbAdapter, null, $resultSetPrototype);
				},
			),
		);
	}

}
