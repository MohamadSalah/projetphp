create database cinedb;

use cinedb;

CREATE TABLE IF NOT EXISTS user (
	id int(10) unsigned NOT NULL AUTO_INCREMENT, 
	username varchar(100) UNIQUE NOT NULL, 
	password varchar(32) NOT NULL, 
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS service (
	id int(10) unsigned NOT NULL AUTO_INCREMENT, 
	type int NOT NULL, 
	start date NOT NULL, 
	expire date NOT NULL,
    usernameUser varchar(100) NOT NULL,
    actualconsumption int(10) NOT NULL, 
	maxconsumption int(10) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS moviehistory (
	id int(10) unsigned NOT NULL AUTO_INCREMENT, 
	title text NOT NULL,
	year text NOT NULL,
	runtime text NOT NULL,
	genre text NOT NULL,
	director text NOT NULL,
	actors text NOT NULL,
	plot text NOT NULL,
	poster text NOT NULL,
	score text NOT NULL,
	idService int(10) NOT NULL, 
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS achat (
	id int(10) unsigned NOT NULL AUTO_INCREMENT, 
	type varchar(100) NOT NULL,
	usernameUser varchar(100) NOT NULL, 
	PRIMARY KEY(id)
);