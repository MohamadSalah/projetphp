<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'CineDB\Controller\user' => 'CineDB\Controller\userController',
			'CineDB\Controller\cinedb' => 'CineDB\Controller\cinedbController',
			'CineDB\Controller\login' => 'CineDB\Controller\LoginController',
			'CineDB\Controller\registration' => 'CineDB\Controller\RegistrationController',
			'CineDB\Controller\service' => 'CineDB\Controller\serviceController',
			'CineDB\Controller\moviehistory' => 'CineDB\Controller\moviehistoryController',
			'CineDB\Controller\achat' => 'CineDB\Controller\achatController',
			),
		),
		'router' => array(
			'routes' => array(
				/*'user' => array(
					'type' => 'segment',
						'options' => array(
							'route' => '/user[/:action][/:id]', // règle de routage
							'constraints' => array(
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id' => '[0-9]+',
						),
						'defaults' => array(
							'controller' => 'CineDB\Controller\user',
							'action' => 'index',
						),
					),
				),*/
				/*'cinedb' => array(
					'type' => 'segment',
						'options' => array(
							'route' => '/cinedb[/:action][/:id]', // règle de routage
							'constraints' => array(
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id' => '[0-9]+',
						),
						'defaults' => array(
							'controller' => 'CineDB\Controller\cinedb',
							'action' => 'index',
						),
					),
				),*/
				'cinedb' => array(
					'type' => 'Literal',
						'options' => array(
							'route' => '/cinedb', // règle de routage
						'defaults' => array(
							'__NAMESPACE__' => 'CineDB\Controller',
							'controller' => 'login',
							'action' => 'index',
						),
					),
					'may_terminate' => true,
					'child_routes' => array(
						'default' => array(
							'type'    => 'Segment',
							'options' => array(
								'route'    => '/[:controller[/:action[/:id]]]',
								'constraints' => array(
									'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
									'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
									'id'     	 => '.*',
								),
								'defaults' => array(
								),
							),
						),
					),
				),
			),
		), 
	'view_manager' => array(
		'template_path_stack' => array(
			'cinedb' => __DIR__ . '/../view',
			),
		),
	'service_manager' => array(
		'aliases' => array(
			'Zend\Authentication\AuthenticationService' => 'my_auth_service',
		),
		'invokables' => array(
			'my_auth_service' => 'Zend\Authentication\AuthenticationService',
		),
	),
	);